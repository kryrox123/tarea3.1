<?php
// Incluye tu función registrarUsuario aquí o importa el archivo donde se encuentra.

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Obtén los valores del formulario
    $nombre = $_POST['registerName'];
    $apellido = $_POST['registerLastName'];
    $email = $_POST['registerEmail'];
    $contraseña = $_POST['registerPass'];

    // Llama a la función registrarUsuario con los valores del formulario
    $resultado = nuevitoUsuario($nombre, $apellido, $email, $contraseña);
    // Aquí puedes realizar más lógica, como redirigir al usuario o mostrar mensajes de éxito/error.
}
?>