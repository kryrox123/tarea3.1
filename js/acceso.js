// Define una función para imprimir todos los usuarios registrados en la página
function imprimirUsuarios() {
    // Supongamos que users es un array con los usuarios registrados (esto debe ser adaptado a tu implementación real)
    var users = [
      { nombre: 'Usuario1', email: 'usuario1@example.com' },
      { nombre: 'Usuario2', email: 'usuario2@example.com' },
      { nombre: 'Usuario3', email: 'usuario3@example.com' },
      // ... otros usuarios ...
    ];
  
    // Encuentra un elemento en la página donde deseas mostrar los usuarios (por ejemplo, un div con el id "usuarios")
    var usuariosDiv = document.getElementById('usuarios');
  
    // Crea un elemento ul (lista desordenada) para mostrar los usuarios
    var ul = document.createElement('ul');
  
    // Itera sobre los usuarios y crea elementos li (elementos de lista) para cada uno
    users.forEach(function(user) {
      var li = document.createElement('li');
      li.textContent = 'Nombre: ' + user.nombre + ', Email: ' + user.email;
      ul.appendChild(li);
    });
  
    // Agrega la lista ul al elemento de la página donde deseas mostrar los usuarios
    usuariosDiv.appendChild(ul);
  }
  
  // Agrega un controlador de eventos para el botón "Imprimir Usuarios"
  document.getElementById('imprimirUsuariosButton').addEventListener('click', imprimirUsuarios);

document.getElementById('imprimirUsuariosButton').addEventListener('click', function () {
    // Llama a la función imprimirUsuarios() definida en acceso.js
    window.location.href = "registro.html";
    imprimirUsuarios();
});

