"use strict"
var usuarios=[
    {
        userName:"Pablo",
        userLastName:"Guzman",
        email:"PGuzman@gmail.com",        
        password:"admin123"
    },
    {
        userName:"Damaris",
        userLastName:"Muños",
        email:"DMuños@gmail.com",        
        password:"vendedor123"
    }
]

function usuarioRegistrado(userName, password){
    Array.prototype.findBy = function (userName, password) {
        for (var i=0; i<this.length; i++) {
            var object = this[i];
            console.log("Usuario: " + object.userName + " " + "Password: " + object.password)
            if ((object.userName == userName) && (object.password == password)) {
                return true
            }
        }
        return false
    }
    return usuarios.findBy(userName, password)
}

function nuevitoUsuario(formulario){
    let nuevoUsuario = {
        userName: formulario.registerName.value,
        userLastName: formulario.registerLastName.value,
        email: formulario.registerEmail.value,
        password: formulario.registerPass.value
    }

    let numUsuario = usuarios.push(nuevoUsuario)
    console.log("Nro de usuario registrado: " + numUsuario)
    alert("Usuario registrado")
    return (numUsuario>0)
}

const validate = (value, type) => {
    let regex, response

    switch (type) {
        case 'text':
            regex = /^[a-zA-Z]+(\s*[a-zA-Z]*)*[a-zA-Z\s]+$/i
            response = {
                validation: regex.test(value),
                message: 'Solo letras'
            }
            /* Reparar esto agregar espacios en blanco */
            if(regex.test(value) && value.length <= 2) {
                response.validation = false
                response.message = 'Debe usar más de dos caracteres'
            }
            return response
        case 'email':
            regex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
            response = {
                validation: regex.test(value),
                message: 'Correo no permitido'
            }
            return response
        case 'tel':
            regex = /^([+|#][0-9])+(\s*[0-9]*){8}$/
            response = {
                validation: regex.test(value),
                message: 'Solo un numero permitido'
            }
            return response
        case 'select-one':
            response = {
                validation: value ? true: false,
                message: 'Debe seleccionar una opcion'
            }
            return response
        case 'password':
            response = {
                validation: true,
                message: 'contraseña'
            }
            return response
        default:
            return false
    }
    
}

const isEmpty = (value) => {
    return value === ''
}

const clearForm = (form) => {

    for(let i = 0; i < form.length; i++) {
        let field = form[i]
         if(field.type != 'submit') { 
            $(field).removeClass('input--visible input--sussess')
            field.value = ''
         } 
    }
}

function registrarUsuario(form) {
    // Obtén los valores de los campos de registro
    var nombre = form.elements["registerName"].value;
    var apellido = form.elements["registerLastName"].value;
    var email = form.elements["registerEmail"].value;
    var contraseña = form.elements["registerPass"].value;
    var repetirContraseña = form.elements["registerRePass"].value;
  
    // Verificar campos vacíos
    if (nombre === "" || apellido === "" || email === "" || contraseña === "" || repetirContraseña === "") {
      // Mostrar mensaje de alerta o messagebox
      alert("USUARIO NO REGISTRADO: Por favor, complete todos los campos.");
      return "USUARIO NO REGISTRADO";
    }
  
    // Verificar si la contraseña y la repetición de la contraseña son diferentes
    if (contraseña !== repetirContraseña) {
      // Mostrar mensaje de alerta o messagebox
      alert("USUARIO NO REGISTRADO: La contraseña no coincide.");
      return "USUARIO NO REGISTRADO";
    }
  
    // En este punto, todos los campos están completos y las contraseñas coinciden.
    // Simula un registro exitoso
    var registroExitoso = true;
  
    if (registroExitoso) {
      // Si el registro fue exitoso, devuelve el mensaje "USUARIO REGISTRADO CON ÉXITO"
      alert("USUARIO REGISTRADO CON ÉXITO !");
      return "USUARIO REGISTRADO CON ÉXITO";
    } else {
      // Si el registro no fue exitoso, devuelve el mensaje "USUARIO NO REGISTRADO"
      alert("USUARIO NO REGISTRADO: Ocurrió un error durante el registro.");
      return "USUARIO NO REGISTRADO";
    }
  }

const $formSignin = document.getElementById("form-signin");
const $formNewletter = document.getElementById("form-newletter");
const $formContact = document.getElementById("form-contact");

$formSignin.addEventListener('submit', async (e) => {
    e.preventDefault()
    const data = new FormData($formSignin)

    let formSize = e.target.length - 1, emptyFileds = [], validateFields = []

    for (let i = 0; i < e.target.length; i++) {

        let field = e.target[i]
        console.log(field.type)
        
        $(field).removeClass('input--visible input--sussess input--danger input--warning')

        if (field.type == 'radio') formSize = formSize - 1
        if (field.type == 'textarea') formSize = formSize - 1
        if (field.type == 'hidden') formSize = formSize - 1

        if(field.type != 'submit' && field.type != 'radio' && field.type != 'textarea' && field.type != 'hidden') {
            $(field).next().click(()=>{
                field.focus()
            })

            if (!(field.value === '')) {
                $(field).removeClass('input--visible input--warning')
                $(field).next().text('')
                emptyFileds.push(field.value)
                
                let res = validate(field.value, field.type)
                console.log(res + "99");

                if(res.validation) {
                    $(field).addClass('input--visible input--sussess')
                    $(field).next().text('')
                    validateFields.push(field.value)
                } else {
                    $(field).addClass('input--visible input--danger')
                    $(field).next().text(res.message)
                }
                
            } else {
                $(field).addClass('input--visible input--warning')
                $(field).next().text('Campo requerido')
            } 
        }
    }

    if( formSize === emptyFileds.length && formSize === validateFields.length) {
        let f = document.getElementById("form-signin")
        if (usuarioRegistrado(f.Username.value, f.Password.value)){
            //document.getElementById("form-signin").submit()
            alert("Bienvenido")
            return true
        }
        else{
            alert("Usuario no existente")
            return false
        }
    }
    else {
        return false
    }
})

$formNewletter.addEventListener('submit', async (e) => {
    e.preventDefault()
    const data = new FormData($formSignin)

    let formSize = e.target.length - 1, emptyFileds = [], validateFields = []

    for (let i = 0; i < e.target.length; i++) {

        let field = e.target[i]
        console.log(field.type)
        
        $(field).removeClass('input--visible input--sussess input--danger input--warning')

        if (field.type == 'radio') formSize = formSize - 1
        if (field.type == 'textarea') formSize = formSize - 1
        if (field.type == 'hidden') formSize = formSize - 1

        if(field.type != 'submit' && field.type != 'radio' && field.type != 'textarea' && field.type != 'hidden') {
            $(field).next().click(()=>{
                field.focus()
            })

            if (!(field.value === '')) {
                $(field).removeClass('input--visible input--warning')
                $(field).next().text('')
                emptyFileds.push(field.value)
                
                let res = validate(field.value, field.type)

                console.log(res+"155");

                if(res.validation) {
                    $(field).addClass('input--visible input--sussess')
                    $(field).next().text('')
                    validateFields.push(field.value)
                } else {
                    $(field).addClass('input--visible input--danger')
                    $(field).next().text(res.message)
                }
                
            } else {
                $(field).addClass('input--visible input--warning')
                $(field).next().text('Campo requerido')
            } 
        }
    }

    if( formSize === emptyFileds.length && formSize === validateFields.length) {
        //document.getElementById("form-newletter").submit();
        clearForm($formNewletter)
        addMessage('Formulario enviado', 'sussess')
        return true
    } else {
        return false
    }
})


$formContact.addEventListener('submit', async (e) => {
    e.preventDefault()
    const data = new FormData($formSignin)

    let formSize = e.target.length - 1, emptyFileds = [], validateFields = []

    for (let i = 0; i < e.target.length; i++) {

        let field = e.target[i]
        console.log(field.type)
        
        $(field).removeClass('input--visible input--sussess input--danger input--warning')

        if (field.type == 'radio') formSize = formSize - 1
        if (field.type == 'textarea') formSize = formSize - 1
        if (field.type == 'hidden') formSize = formSize - 1

        if(field.type != 'submit' && field.type != 'radio' && field.type != 'textarea' && field.type != 'hidden') {
            $(field).next().click(()=>{
                field.focus()
            })

            if (!(field.value === '')) {
                $(field).removeClass('input--visible input--warning')
                $(field).next().text('')
                emptyFileds.push(field.value)
                
                let res = validate(field.value, field.type)

                console.log(res+"213");

                if(res.validation) {
                    $(field).addClass('input--visible input--sussess')
                    $(field).next().text('')
                    validateFields.push(field.value)
                } else {
                    $(field).addClass('input--visible input--danger')
                    $(field).next().text(res.message)
                }
                
            } else {
                $(field).addClass('input--visible input--warning')
                $(field).next().text('Campo requerido')
            } 
        }
    }

    if( formSize === emptyFileds.length && formSize === validateFields.length) {
        //document.getElementById("form-contact").submit();
        clearForm($formContact)
        addMessage('Formulario enviado', 'sussess')
        return true
    } else {
        return false
    }
})

